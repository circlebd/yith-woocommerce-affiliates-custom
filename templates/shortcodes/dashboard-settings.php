<?php

/**
 * Affiliate Dashboard Settings
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Affiliates
 * @version 1.0.5
 */

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if (!defined('YITH_WCAF')) {
	exit;
} // Exit if accessed directly

$show_right_column = true;
$show_left_column = true;

$user_id       = get_current_user_id();

$payment_method = get_user_meta($user_id, 'aff_payment_method', true);
$payment_bkash = get_user_meta($user_id, 'aff_payment_bkash', true);
$payment_nagad = get_user_meta($user_id, 'aff_payment_nagad', true);
$payment_rocket = get_user_meta($user_id, 'aff_payment_rocket', true);

?>

<div class="yith-wcaf yith-wcaf-settings woocommerce">

	<?php
	if (function_exists('wc_print_notices')) {
		wc_print_notices();
	}
	?>

	<?php do_action('yith_wcaf_before_dashboard_section', 'settings'); ?>

	<div class="left-column <?php echo (!$show_right_column) ? 'full-width' : ''; ?>">

		<form method="post">

			<?php do_action('yith_wcaf_settings_form_start'); ?>

			<?php if (apply_filters('yith_wcaf_show_additional_fields', 'yes' === $show_additional_fields, 'settings')) : ?>

				<?php if ('yes' === $show_name_field) : ?>
					<p class="form form-row">
						<label for="first_name"><?php esc_html_e('First name', 'yith-woocommerce-affiliates'); ?></label>
						<input type="text" name="first_name" id="first_name" value="<?php echo esc_attr($affiliate_name); ?>" />
						<small><?php esc_html_e('(First name for your account)', 'yith-woocommerce-affiliates'); ?></small>
					</p>
				<?php endif; ?>

				<?php if ('yes' === $show_surname_field) : ?>
					<p class="form form-row">
						<label for="last_name"><?php esc_html_e('Last name', 'yith-woocommerce-affiliates'); ?></label>
						<input type="text" name="last_name" id="first_name" value="<?php echo esc_attr($affiliate_surname); ?>" />
						<small><?php esc_html_e('(Last name for your account)', 'yith-woocommerce-affiliates'); ?></small>
					</p>
				<?php endif; ?>

			<?php endif; ?>

			<p class="form">
				<label for="payment_method"><?php echo apply_filters('yith_wcaf_payment_bkash_dashboard_settings', __('Payment Method', 'yith-woocommerce-affiliates')); ?></label>
				<select name="payment_method" id="payment_method">
					<option value="bkash" <?php selected($payment_method, 'bkash'); ?>>bKash</option>
					<option value="nagad" <?php selected($payment_method, 'nagad'); ?>>Nagad</option>
					<option value="rocket" <?php selected($payment_method, 'rocket'); ?>>Rocket</option>
					<option value="paypal" <?php selected($payment_method, 'paypal'); ?>>Paypal</option>
				</select>
				<br>
				<small><?php esc_html_e('Please select a payment method from the list', 'yith-woocommerce-affiliates'); ?></small>
			</p>

			<?php if (apply_filters('yith_wcaf_payment_bkash_required', true)) : ?>
				<p class="form yith_wcaf_payment_method" id="payment_bkash">
					<label for="payment_bkash"><?php echo apply_filters('yith_wcaf_payment_bkash_dashboard_settings', __('bKash Number', 'yith-woocommerce-affiliates')); ?></label>
					<input type="text" name="payment_bkash" id="payment_bkash" value="<?php echo esc_attr($payment_bkash); ?>" /><br>
					<small><?php esc_html_e('Please enter your bkash Number to receive commissions', 'yith-woocommerce-affiliates'); ?></small>
				</p>
			<?php endif; ?>

			<?php if (apply_filters('yith_wcaf_payment_nagad_required', true)) : ?>
				<p class="form yith_wcaf_payment_method" id="payment_nagad">
					<label for="payment_nagad"><?php echo apply_filters('yith_wcaf_payment_nagad_dashboard_settings', __('Nagad Number', 'yith-woocommerce-affiliates')); ?></label>
					<input type="text" name="payment_nagad" id="payment_nagad" value="<?php echo esc_attr($payment_nagad); ?>" /><br>
					<small><?php esc_html_e('Please enter your Nagad Number to receive commissions', 'yith-woocommerce-affiliates'); ?></small>
				</p>
			<?php endif; ?>

			<?php if (apply_filters('yith_wcaf_payment_rocket_required', true)) : ?>
				<p class="form yith_wcaf_payment_method" id="payment_rocket">
					<label for="payment_rocket"><?php echo apply_filters('yith_wcaf_payment_rocket_dashboard_settings', __('Rocket Number', 'yith-woocommerce-affiliates')); ?></label>
					<input type="text" name="payment_rocket" id="payment_rocket" value="<?php echo esc_attr($payment_rocket); ?>" /><br>
					<small><?php esc_html_e('Please enter your Rocket Number to receive commissions', 'yith-woocommerce-affiliates'); ?></small>
				</p>
			<?php endif; ?>

			<?php if (apply_filters('yith_wcaf_payment_email_required', true)) : ?>
				<p class="form yith_wcaf_payment_method" id="payment_email">
					<label for="payment_email"><?php echo apply_filters('yith_wcaf_payment_email_dashboard_settings', __('Payment email', 'yith-woocommerce-affiliates')); ?></label>
					<input type="email" name="payment_email" id="payment_email" value="<?php echo esc_attr($payment_email); ?>" /><br>
					<small><?php esc_html_e('Email address where you want to receive PayPal payments for commissions', 'yith-woocommerce-affiliates'); ?></small>
				</p>
			<?php endif; ?>

			<?php do_action('yith_wcaf_settings_form_after_payment_email'); ?>

			<?php do_action('yith_wcaf_settings_form'); ?>

			<input type="submit" name="settings_submit" value="<?php esc_attr_e('Submit', 'yith-woocommerce-affiliates'); ?>" />

		</form>

	</div>

	<!--NAVIGATION MENU-->
	<?php
	$atts = array(
		'show_right_column'    => $show_right_column,
		'show_left_column'     => $show_left_column,
		'show_dashboard_links' => $show_dashboard_links,
		'dashboard_links'      => $dashboard_links,
	);
	yith_wcaf_get_template('navigation-menu.php', $atts, 'shortcodes');
	?>

	<?php do_action('yith_wcaf_after_dashboard_section', 'settings'); ?>

</div>