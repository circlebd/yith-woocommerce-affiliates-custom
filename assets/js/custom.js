jQuery(document).ready(function ($) {
  make_payment_method_visible($("#payment_method").val());
  $("#payment_method").on("change", function () {
    var method = $(this).val();
    $(".yith_wcaf_payment_method").hide();
    make_payment_method_visible(method);
  });

  function make_payment_method_visible(method = "bkash") {
    switch (method) {
      case "bkash":
        $(".yith_wcaf_payment_method#payment_bkash").show();
        break;
      case "nagad":
        $(".yith_wcaf_payment_method#payment_nagad").show();
        break;
      case "rocket":
        $(".yith_wcaf_payment_method#payment_rocket").show();
        break;
      case "paypal":
        $(".yith_wcaf_payment_method#payment_email").show();
        break;
    }
  }
});
